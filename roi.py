import cv2
import math


class DotDict(dict):
    __getattr__ = dict.__getitem__
    __setattr__ = dict.__setitem__
    __delattr__ = dict.__delitem__


class ContourMeasurements:
    orientation = None
    horizon = None
    relative_center = None

    @staticmethod
    def image_keypoints(image, proportion: float = 0.1) -> DotDict:
        h, w = image.shape[:2]

        return DotDict(
            center=tuple(map(int, (w / 2, h / 2))),
            left=tuple(map(int, (0, h / 2))),
            right=tuple(map(int, (w, h / 2))),
            top=tuple(map(int, (w / 2, 0))),
            bottom=tuple(map(int, (w / 2, h))),
            top_left_10=tuple(map(int, (0 + proportion * w, 0 + proportion * h))),
            bot_right_10=tuple(map(int, (w - proportion * w, h - proportion * h)))
        )

    @staticmethod
    def pythagorean_distance(points: tuple) -> float:
        x, y = points[0], points[1]
        dist = math.hypot(y[0] - x[0], y[1] - x[1])
        return dist

    @staticmethod
    def line_to_point(cnt, location) -> tuple:
        min_dist = math.inf
        result = None
        for point in cnt:
            point = tuple(point[0])
            dist = ContourMeasurements.pythagorean_distance((point, location))
            if dist < min_dist:
                result = (point, location)
                min_dist = dist
        return result

    def __init__(self, cnt, keypoints: DotDict):
        self.__find_orientation(cnt, keypoints)
        self.__find_horizon(cnt, keypoints)

    def __find_orientation(self, cnt, keypoints: DotDict):
        left_dist = ContourMeasurements.pythagorean_distance(ContourMeasurements.line_to_point(cnt, keypoints.left))
        right_dist = ContourMeasurements.pythagorean_distance(ContourMeasurements.line_to_point(cnt, keypoints.right))

        if left_dist > right_dist:
            self.orientation = 'right'
        else:
            self.orientation = 'left'

    def __find_horizon(self, cnt, keypoints):
        top_point, _ = ContourMeasurements.line_to_point(cnt, keypoints.top)
        bot_point, _ = ContourMeasurements.line_to_point(cnt, keypoints.bottom)

        horizon_x = int(top_point[0] + (bot_point[0] - top_point[0]) / 2)
        horizon_y = int(top_point[1] + (bot_point[1] - top_point[1]) / 2)

        self.horizon = (horizon_x, horizon_y)
        self.relative_center = (int(keypoints.center[0]), horizon_y)


if __name__ == '__main__':
    import matplotlib
    import matplotlib.pyplot as plt

    matplotlib.use('QT4Agg')

    _image_c = cv2.imread('contours.png', 1)
    _image = cv2.imread('contours.png', 0)

    _, thresh = cv2.threshold(_image, 127, 255, 0)
    _, contours, _ = cv2.findContours(thresh, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_TC89_KCOS)

    _keypoints = ContourMeasurements.image_keypoints(_image_c, 0.05)

    for key, value in _keypoints.items():
        cv2.circle(_image_c, value, 3, (255, 0, 0), 1)

    for _cnt in contours:
        _line = ContourMeasurements.line_to_point(_cnt, _keypoints.center)
        cv2.line(_image_c, _line[0], _line[1], (0, 255, 0), 1)

        _cnt_attrs = ContourMeasurements(_cnt, _keypoints)
        cv2.putText(_image_c, _cnt_attrs.orientation, _line[0], cv2.FONT_HERSHEY_SIMPLEX, 0.5, 255)
        cv2.circle(_image_c, _cnt_attrs.horizon, 3, (125, 125, 255), 1)
        cv2.circle(_image_c, _cnt_attrs.relative_center, 3, (125, 0, 255), 1)
        cv2.line(_image_c, _cnt_attrs.horizon, _cnt_attrs.relative_center, (125, 0, 255), 1)

        for _point in _cnt:
            _point = tuple(_point[0])
            cv2.circle(_image_c, _point, 1, (0, 0, 255), 1)
        # cv2.drawContours(image_c, [cnt], -1, (0, 0, 255), 1)

    plt.imshow(_image_c)
    mng = plt.get_current_fig_manager()
    mng.window.showMaximized()  # QT4Agg
    plt.show()
